# GITLAB CI
Pipelines, Continuous Delivery e Deployment
# Instalação

Exemplos baseados no sistema operacional MacOS Catalina

NodeJS 8 (ou superior)

```sh
brew install node
```

Docusaurus
```sh
npm  install --global docusaurus-init
```
# Criando projeto do zero

- crie um diretório
```sh
mkdir nome_diretorio
```

- entre no diretório criado
```sh
cd nome_diretorio
```

- agora criaremos o template do projeto docusaurus através do comando
```sh
docusaurus-init
```
# Rodando projeto

Para rodar o projeto entre dentro do diretório `website` e rode o projeto usando npm

```
cd website

npm run start
```
# Build
O build do projeto converte os arquivos mark down (.md) para conteúdo web (HTML, CSS, JS). Dentro do diretório `/website` rode o comando
```sh
    npm run build
```
# Publicação

Para publicar o site utilizaremos a ferramenta [surge](https://surge.sh/). 
## Instalando surge

```sh
npm install --global surge

```

Ao rodar o comando `surge` será solicitado o login (para recuparar a senha erre a senha 3 vezes e receberá um email para resetar a senha).

Entre com suas credenciais ou efetue o cadastro. Em seguida será solicitado o diretório onde está o projeto (arquivos estáticos), escolha um dominio e pronto, o surge se encarregará do resto.

Veja o exemplo a baixo

```sh
 surge     

   Running as rafael.costab@hotmail.com (Student)

        project: /Users/rafaelcostab/Desktop/missaodevops/website/build/missaodevops-site
         domain: missaodevops-site.surge.sh
         upload: [====================] 100% eta: 0.0s (55 files, 537510 bytes)
            CDN: [====================] 100%
     encryption: *.surge.sh, surge.sh (100 days)
             IP: 138.197.235.123

   Success! - Published to missaodevops-site.surge.sh
```

# Testes

Executaremos alguns testes para validar se há links quebrados no site, para isso usaremos um recurso chamado `blokenlink checker`, o mesmo vascula todos os links chamados e verifica se há algum link que não esteja carregando.

```sh
sudo npm install -global broken-link-checker
```

Efetuando teste
```sh
blc --recursive --exclude-external http://localhost:3000/
```

wait-on utilizaremos para fazer o terminal esperar uma resposta da execução de um processo
```sh
sudo npm install -global wait-on
```

### Analise de vulnerabilidades

Utilizaremos o [Synk](https://snyk.io/) que é uma ferramenta que encontra vulnerabilidades de segurança no projeto

```sh
npm install -global snyk
```
para rodar os teste e verificar vulnerabilidades utilize o comando

```sh
snyk test
```